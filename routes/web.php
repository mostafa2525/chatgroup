<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();



Route::get('/', 'ChatsController@index');
Route::get('/allmessages', 'ChatsController@fetchMessages')->name("front.get.messages");
Route::post('/sendMessage', 'ChatsController@sendMessage')->name("front.post.messages");
